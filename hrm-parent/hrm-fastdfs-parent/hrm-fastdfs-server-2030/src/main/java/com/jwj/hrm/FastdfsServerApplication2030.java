package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 文件服务启动类
 */
@SpringBootApplication
public class FastdfsServerApplication2030
{
    public static void main( String[] args )
    {
        SpringApplication.run(FastdfsServerApplication2030.class);
    }
}

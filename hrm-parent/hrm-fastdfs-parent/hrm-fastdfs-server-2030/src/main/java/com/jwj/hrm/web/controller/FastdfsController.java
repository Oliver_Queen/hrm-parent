package com.jwj.hrm.web.controller;

import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.utils.FastDfsApiOpr;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FastdfsController {

    @RequestMapping(value = "/fastdfs/upload",method = RequestMethod.POST)
    public AjaxResult upload(MultipartFile file) throws IOException {

        //获取文件扩展名
        String extName = FilenameUtils.getExtension(file.getOriginalFilename());

        //使用工具类上传文件到服务器
        String filePath = FastDfsApiOpr.upload(file.getBytes(), extName);

        return AjaxResult.me().setResultObj(filePath);
    }



    //文件删除
    @RequestMapping(value = "/fastdfs/remove",method = RequestMethod.POST)
    public AjaxResult remove(@RequestParam(value = "path") String filepath) throws IOException {


        int first = filepath.indexOf("/");
        int i = filepath.indexOf("/", first + 1);
        String groupName = filepath.substring(first + 1, i);
        String fileName = filepath.substring(i + 1);
        FastDfsApiOpr.delete(groupName,fileName);
        return AjaxResult.me();
    }



}

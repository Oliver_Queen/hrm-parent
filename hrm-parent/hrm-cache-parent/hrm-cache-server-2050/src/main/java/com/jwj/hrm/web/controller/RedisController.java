package com.jwj.hrm.web.controller;

import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.utils.RedisUtils;
import org.springframework.web.bind.annotation.*;

@RestController
public class RedisController {

    //保存值
    @RequestMapping(value = "/redis/set",method = RequestMethod.POST)
    public AjaxResult setString(@RequestParam("key") String key , @RequestParam("value")String value){
        RedisUtils.INSTANCE.set(key,value);
        return AjaxResult.me();
    }

    @RequestMapping(value = "/redis/setex",method = RequestMethod.POST)
    public AjaxResult setex(@RequestParam("key") String key ,
                            @RequestParam("seconds")int seconds,
                            @RequestParam("value")String value){
        RedisUtils.INSTANCE.setex(key,seconds,value);
        return AjaxResult.me();
    }

    //获取值
    @RequestMapping(value = "/redis/get/{key}",method = RequestMethod.GET)
    public AjaxResult getString(@PathVariable("key") String key){
        String result = RedisUtils.INSTANCE.get(key);
        return AjaxResult.me().setResultObj(result);
    }

}

package com.jwj.hrm.fallback;


import com.jwj.hrm.feignclient.RedisFeignClient;
import com.jwj.hrm.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

//托底方法
@Component
public class RedisFeignClientFallbackFactory implements FallbackFactory<RedisFeignClient>{

    @Override
    public RedisFeignClient create(Throwable throwable) {
        return new RedisFeignClient() {
            @Override
            public AjaxResult setString(String key, String value) {
                throwable.printStackTrace();
                return AjaxResult.me().setSuccess(false).setMessage("缓存服务不可用["+throwable.getMessage()+"]");
            }

            @Override
            public AjaxResult getString(String key) {
                throwable.printStackTrace();
                return AjaxResult.me().setSuccess(false).setMessage("缓存服务不可用["+throwable.getMessage()+"]");
            }

            @Override
            public AjaxResult setex(String key, int seconds, String value) {
                throwable.printStackTrace();
                return AjaxResult.me().setSuccess(false).setMessage("缓存服务不可用["+throwable.getMessage()+"]");
            }
        };
    }
}

package com.jwj.hrm.feignclient;


import com.jwj.hrm.fallback.RedisFeignClientFallbackFactory;
import com.jwj.hrm.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

//用来调用缓存服务的Feign接口
@FeignClient(value = "cache-server",fallbackFactory = RedisFeignClientFallbackFactory.class)
public interface RedisFeignClient {

    //保存值
    @RequestMapping(value = "/redis/set",method = RequestMethod.POST)
    AjaxResult setString(@RequestParam("key") String key, @RequestParam("value") String value);

    //获取值
    @RequestMapping(value = "/redis/get/{key}",method = RequestMethod.GET)
    AjaxResult getString(@PathVariable("key") String key);


    @RequestMapping(value = "/redis/setex",method = RequestMethod.POST)
    AjaxResult setex(@RequestParam("key") String key ,
                     @RequestParam("seconds")int seconds,
                     @RequestParam("value")String value);

}

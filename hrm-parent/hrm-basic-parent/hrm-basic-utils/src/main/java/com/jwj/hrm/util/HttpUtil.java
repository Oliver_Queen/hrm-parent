package com.jwj.hrm.util;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.util.*;

public class HttpUtil {

    //post请求 "http://gbk.api.smschinese.cn"
    public static String sendPost(String url, Map<String,String> param ){
        try{
            HttpClient client = new HttpClient ();
            PostMethod post = new PostMethod(url);
            post.addRequestHeader("Content-Type",
                    "application/x-www-form-urlencoded;charset=utf-8");//在头文件中设置转码

            //封装参数

            Set<String> keys = param.keySet();

            List<NameValuePair> nameValuePairs = new ArrayList<>();

            for(String key : keys){
                NameValuePair nameValuePair = new NameValuePair(key, param.get(key));
                nameValuePairs.add(nameValuePair);
            }
            post.setRequestBody(nameValuePairs.toArray(new NameValuePair[]{}));

            client.executeMethod(post);
            int statusCode = post.getStatusCode();
            System.out.println("statusCode:"+statusCode);
            String result = new String(post.getResponseBodyAsString().getBytes("utf-8"));
            System.out.println(result); //打印返回消息状态
            post.releaseConnection();

            return result;
        }catch (Exception e){
            throw new RuntimeException("HTTP请求失败");
        }
    }
}

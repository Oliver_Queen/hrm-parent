package com.jwj.hrm.dto;

import com.jwj.hrm.domain.Employee;
import com.jwj.hrm.domain.Tenant;

public class TenantDto {

    //机构对象
    private Tenant tenant;

    //管理员对象
    private Employee employee;

    //套餐ID
    private Long mealId;

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getMealId() {
        return mealId;
    }

    public void setMealId(Long mealId) {
        this.mealId = mealId;
    }
}
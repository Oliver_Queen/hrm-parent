package com.jwj.hrm.service;

import com.jwj.hrm.domain.Department;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
public interface IDepartmentService extends IService<Department> {

}

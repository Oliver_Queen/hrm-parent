package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Department;
import com.jwj.hrm.mapper.DepartmentMapper;
import com.jwj.hrm.service.IDepartmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}

package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Employee;
import com.jwj.hrm.domain.Tenant;
import com.jwj.hrm.dto.TenantDto;
import com.jwj.hrm.mapper.EmployeeMapper;
import com.jwj.hrm.mapper.TenantMapper;
import com.jwj.hrm.service.ITenantService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public void entering(TenantDto tenantDto) {
        Tenant tenant = tenantDto.getTenant();
        Employee employee = tenantDto.getEmployee();
        Long mealId = tenantDto.getMealId();

        //判断基本参数
        if(!StringUtils.hasLength(tenant.getCompanyName())){
            throw new RuntimeException("公司名字不能为空");
        }

        //保存租户 获取到租户ID
        Date date = new Date();
        tenant.setRegisterTime(date);
        tenant.setState(false);
        baseMapper.insert(tenant);

        Long tenantId = tenant.getId();
        employee.setInputTime(date);
        //员工状态
        employee.setState(Employee.STATE_NORMAL);
        employee.setTenantId(tenantId);
        //员工类型
        employee.setType(Employee.TYPE_ADMIN);
        employeeMapper.insert(employee);

        //保存租户和套餐关系
        baseMapper.insertRelationWithMeal(mealId ,tenantId ,Tenant.STATE_MEAL_UNPAY );
    }
}

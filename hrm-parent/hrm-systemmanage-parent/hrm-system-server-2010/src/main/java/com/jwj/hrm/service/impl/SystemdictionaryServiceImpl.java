package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Systemdictionary;
import com.jwj.hrm.domain.Systemdictionaryitem;
import com.jwj.hrm.mapper.SystemdictionaryMapper;
import com.jwj.hrm.service.ISystemdictionaryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
@Service
public class SystemdictionaryServiceImpl extends ServiceImpl<SystemdictionaryMapper, Systemdictionary> implements ISystemdictionaryService {


}

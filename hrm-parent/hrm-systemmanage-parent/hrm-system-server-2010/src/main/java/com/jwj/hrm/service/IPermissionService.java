package com.jwj.hrm.service;

import com.jwj.hrm.domain.Permission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
public interface IPermissionService extends IService<Permission> {

}

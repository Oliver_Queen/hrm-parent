package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 系统管理服务启动类
 */
@SpringBootApplication
//@EnableEurekaClient
public class SystemServerApplication2010
{
    public static void main( String[] args )
    {
        SpringApplication.run(SystemServerApplication2010.class);
    }
}

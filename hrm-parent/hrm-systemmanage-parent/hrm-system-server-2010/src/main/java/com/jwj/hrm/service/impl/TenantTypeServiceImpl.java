package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.TenantType;
import com.jwj.hrm.mapper.TenantTypeMapper;
import com.jwj.hrm.service.ITenantTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 租户(机构)类型表 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
@Service
public class TenantTypeServiceImpl extends ServiceImpl<TenantTypeMapper, TenantType> implements ITenantTypeService {

}

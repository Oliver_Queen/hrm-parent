package com.jwj.hrm.service;

import com.jwj.hrm.domain.TenantType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 租户(机构)类型表 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface ITenantTypeService extends IService<TenantType> {

}

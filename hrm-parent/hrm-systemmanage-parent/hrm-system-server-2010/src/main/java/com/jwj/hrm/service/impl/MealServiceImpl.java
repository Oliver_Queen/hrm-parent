package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Meal;
import com.jwj.hrm.mapper.MealMapper;
import com.jwj.hrm.service.IMealService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class MealServiceImpl extends ServiceImpl<MealMapper, Meal> implements IMealService {

}

package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Employee;
import com.jwj.hrm.mapper.EmployeeMapper;
import com.jwj.hrm.service.IEmployeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

}

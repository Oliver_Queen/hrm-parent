package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Employee;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}

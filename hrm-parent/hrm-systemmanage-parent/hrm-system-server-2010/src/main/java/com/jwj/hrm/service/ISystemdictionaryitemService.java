package com.jwj.hrm.service;

import com.jwj.hrm.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface ISystemdictionaryitemService extends IService<Systemdictionaryitem> {

    //根据sn查询数据字典明细列表
    List<Systemdictionaryitem> selectListBySn(String sn);
}

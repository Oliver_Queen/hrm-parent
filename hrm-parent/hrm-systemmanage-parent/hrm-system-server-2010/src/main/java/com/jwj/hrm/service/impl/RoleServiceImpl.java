package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Role;
import com.jwj.hrm.mapper.RoleMapper;
import com.jwj.hrm.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}

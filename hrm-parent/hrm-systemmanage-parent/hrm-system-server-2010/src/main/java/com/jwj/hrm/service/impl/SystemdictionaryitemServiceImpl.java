package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Systemdictionaryitem;
import com.jwj.hrm.mapper.SystemdictionaryitemMapper;
import com.jwj.hrm.service.ISystemdictionaryitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
@Service
public class SystemdictionaryitemServiceImpl extends ServiceImpl<SystemdictionaryitemMapper, Systemdictionaryitem> implements ISystemdictionaryitemService {

    @Override
    public List<Systemdictionaryitem> selectListBySn(String sn) {
        return baseMapper.selectListBySn(sn);
    }
}

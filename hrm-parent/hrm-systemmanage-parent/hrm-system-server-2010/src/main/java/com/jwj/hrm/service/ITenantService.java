package com.jwj.hrm.service;

import com.jwj.hrm.domain.Tenant;
import com.baomidou.mybatisplus.service.IService;
import com.jwj.hrm.dto.TenantDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
public interface ITenantService extends IService<Tenant> {

    //机构入住
    void entering(TenantDto tenantDto);
}

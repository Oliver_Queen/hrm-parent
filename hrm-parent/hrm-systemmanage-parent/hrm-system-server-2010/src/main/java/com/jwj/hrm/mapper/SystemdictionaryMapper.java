package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Systemdictionary;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jwj.hrm.domain.Systemdictionaryitem;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface SystemdictionaryMapper extends BaseMapper<Systemdictionary> {


}

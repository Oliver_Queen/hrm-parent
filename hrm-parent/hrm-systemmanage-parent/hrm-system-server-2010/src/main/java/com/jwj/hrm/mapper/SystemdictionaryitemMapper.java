package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface SystemdictionaryitemMapper extends BaseMapper<Systemdictionaryitem> {

    List<Systemdictionaryitem> selectListBySn(String sn);
}

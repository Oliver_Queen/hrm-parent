package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.Menu;
import com.jwj.hrm.mapper.MenuMapper;
import com.jwj.hrm.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}

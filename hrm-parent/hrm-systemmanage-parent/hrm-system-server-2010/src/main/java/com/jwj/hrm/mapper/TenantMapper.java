package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Tenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-26
 */
public interface TenantMapper extends BaseMapper<Tenant> {

    //保存机构和套餐的关系
    void insertRelationWithMeal(@Param("mealId") Long mealId,  @Param("tenantId")Long tenantId,@Param("state") int state);
}

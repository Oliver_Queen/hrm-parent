package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.TenantType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 租户(机构)类型表 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

}

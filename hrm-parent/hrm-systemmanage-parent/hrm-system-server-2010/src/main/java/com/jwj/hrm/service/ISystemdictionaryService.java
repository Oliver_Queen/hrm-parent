package com.jwj.hrm.service;

import com.jwj.hrm.domain.Systemdictionary;
import com.baomidou.mybatisplus.service.IService;
import com.jwj.hrm.domain.Systemdictionaryitem;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface ISystemdictionaryService extends IService<Systemdictionary> {


}

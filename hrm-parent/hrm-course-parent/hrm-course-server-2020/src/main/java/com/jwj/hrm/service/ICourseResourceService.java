package com.jwj.hrm.service;

import com.jwj.hrm.domain.CourseResource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface ICourseResourceService extends IService<CourseResource> {

}

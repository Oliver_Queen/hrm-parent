package com.jwj.hrm.service;

import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.domain.Course;
import com.baomidou.mybatisplus.service.IService;
import com.jwj.hrm.dto.CourseDto;
import com.jwj.hrm.query.CourseQuery;
import com.jwj.hrm.util.PageList;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface ICourseService extends IService<Course> {
    //保存课程
    void saveOrUpdate(CourseDto courseDto);

    //课程上线
    void onLineCourse(Long id);
    //课程下线
    void offLineCourse(Long id);

    PageList<CourseDoc> queryCourses(CourseQuery courseQuery);
}

package com.jwj.hrm.service;

import com.jwj.hrm.domain.CourseType;
import com.baomidou.mybatisplus.service.IService;
import com.jwj.hrm.vo.CrumbsVo;

import java.util.List;

/**
 * <p>
 * 课程目录 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface ICourseTypeService extends IService<CourseType> {

    List<CourseType> treeData();

    List<CrumbsVo> crumbs(Long courseTypeId);
}

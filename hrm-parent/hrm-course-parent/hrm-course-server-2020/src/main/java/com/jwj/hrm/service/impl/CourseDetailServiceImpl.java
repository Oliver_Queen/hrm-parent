package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.CourseDetail;
import com.jwj.hrm.mapper.CourseDetailMapper;
import com.jwj.hrm.service.ICourseDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
@Service
public class CourseDetailServiceImpl extends ServiceImpl<CourseDetailMapper, CourseDetail> implements ICourseDetailService {

}

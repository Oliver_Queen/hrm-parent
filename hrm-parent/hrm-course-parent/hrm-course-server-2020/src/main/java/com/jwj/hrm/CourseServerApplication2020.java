package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 课程服务启动类
 */
@SpringBootApplication
@EnableFeignClients(value="com.jwj.hrm.feignclient")
public class CourseServerApplication2020
{
    public static void main( String[] args )
    {
        SpringApplication.run(CourseServerApplication2020.class);
    }
}

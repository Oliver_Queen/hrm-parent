package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.CourseMarket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface CourseMarketMapper extends BaseMapper<CourseMarket> {

}

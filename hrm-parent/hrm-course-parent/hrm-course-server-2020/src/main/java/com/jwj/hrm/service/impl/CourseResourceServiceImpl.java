package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.CourseResource;
import com.jwj.hrm.mapper.CourseResourceMapper;
import com.jwj.hrm.service.ICourseResourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
@Service
public class CourseResourceServiceImpl extends ServiceImpl<CourseResourceMapper, CourseResource> implements ICourseResourceService {

}

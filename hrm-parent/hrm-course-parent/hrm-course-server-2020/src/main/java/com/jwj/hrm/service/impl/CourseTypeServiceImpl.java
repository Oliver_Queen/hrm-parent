package com.jwj.hrm.service.impl;

import com.alibaba.fastjson.JSON;
import com.jwj.hrm.constants.RedisConstantis;
import com.jwj.hrm.domain.CourseType;
import com.jwj.hrm.feignclient.RedisFeignClient;
import com.jwj.hrm.mapper.CourseTypeMapper;
import com.jwj.hrm.service.ICourseTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.vo.CrumbsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程目录 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
@Service
public class CourseTypeServiceImpl extends ServiceImpl<CourseTypeMapper, CourseType> implements ICourseTypeService {

    @Autowired
    private RedisFeignClient redisFeignClient;

    private List<CourseType> selectCourseTypeFromCache(){
        AjaxResult allCourseTypeResult = redisFeignClient.getString(RedisConstantis.KEY_ALLCOURSETYPE);
        //从redis缓存中查询数据
        if (allCourseTypeResult.isSuccess() && allCourseTypeResult.getResultObj() != null){
            //json格式的缓存数据
            String jsonString = allCourseTypeResult.getResultObj().toString();
            //如果查询到数据直接返回
            List<CourseType> courseTypesFromRedis = JSON.parseArray(jsonString, CourseType.class);
            return courseTypesFromRedis;

        }else {  //缓存中没有就去数据库中查询
            List<CourseType> allCourseType = baseMapper.selectList(null);
            //把查询结果缓存到redis中
            AjaxResult allCourseTypeSetResult = redisFeignClient.setString(RedisConstantis.KEY_ALLCOURSETYPE, JSON.toJSONString(allCourseType));

            return allCourseType;
        }

    }


    @Override
    public List<CourseType> treeData() {


        //1.查询出所有的课程分类
        //加入缓存逻辑
        List<CourseType> allCourseType = selectCourseTypeFromCache();

        //2.从所有的分类中，查询出一级分类
        //一级分类集合
        List<CourseType> parentCourseTypes = new ArrayList<>() ;

        for(CourseType currentCourseType : allCourseType){

            //3.如果某个分类的 pid == 0,那么这个分类就是一级分类
            if(currentCourseType.getPid() == 0){
                parentCourseTypes.add(currentCourseType);
            }else{

                //4.如果不是一级分类，就去找自己的父分类，加入父分类的children
                for(CourseType parentCourseType : allCourseType){

                    //如果当前分类的pid等于某个分类的id,那么这个分类就是它的父分类
                    if(currentCourseType.getPid().equals(parentCourseType.getId())){
                        //把currentCourseType 当前分类加入 父分类parentCourseType的 children
                        parentCourseType.getChildren().add(currentCourseType);
                        break;
                    }
                }
            }
        }
        return parentCourseTypes;
    }

    @Override
    public List<CrumbsVo> crumbs(Long courseTypeId) {

        //根据分类ID查询分类对象
        CourseType courseType = baseMapper.selectById(courseTypeId);
        //获取当前分类的path
        String path = courseType.getPath();

        //去掉两边的 “.”
        path = path.startsWith(".")?path.substring(1) : path;
        path = path.endsWith(".")?path.substring(0,path.length() -1 ) : path;

        String[] courseTypeIds = path.split("\\.");

        List<CrumbsVo> result = new ArrayList<>();

        for(String courseTypeIdStr : courseTypeIds){

            CrumbsVo vo = new CrumbsVo();
            //1037
            Long currentCourseTypeId  = Long.valueOf(courseTypeIdStr) ;

            //查询当前遍历到的分类的对象
            CourseType currentCourseType = baseMapper.selectById(currentCourseTypeId);

            //把当前分类装到 CrumbsVo.ownerProductType
            vo.setOwnerProductType(currentCourseType);

            //根据当前遍历到的分类查找其兄弟列表，pid相同的就是兄弟列表
            List<CourseType>  otherProductTypes = baseMapper.selectByPid(currentCourseType.getPid(),currentCourseType.getId());
            //把兄弟列表放到CrumbsVo.otherProductTypes
            vo.setOtherProductTypes(otherProductTypes);

            //把所有的CrumbsVo装到List
            result.add(vo);
        }
        return result;
    }


    @Override
    public boolean insert(CourseType entity) {
        boolean result = super.insert(entity);

        if (result){

            //添加成功  重置redis
            List<CourseType> allCourseType = baseMapper.selectList(null);
            //把可以分类缓存到Redis中
            AjaxResult allCourseTypeSetResult =
                    redisFeignClient.setString(RedisConstantis.KEY_ALLCOURSETYPE, JSON.toJSONString(allCourseType));
            if(!allCourseTypeSetResult.isSuccess()){
                throw new RuntimeException("缓存重置失败");
            }

        }

        return result;
    }

    @Override
    public boolean updateById(CourseType entity) {
        boolean result = super.updateById(entity);
        if (result){

            //添加成功  重置redis
            List<CourseType> allCourseType = baseMapper.selectList(null);
            //把可以分类缓存到Redis中
            AjaxResult allCourseTypeSetResult =
                    redisFeignClient.setString(RedisConstantis.KEY_ALLCOURSETYPE, JSON.toJSONString(allCourseType));
            if(!allCourseTypeSetResult.isSuccess()){
                throw new RuntimeException("缓存重置失败");
            }

        }

        return result;

    }

    @Override
    public boolean deleteById(Serializable id) {

        boolean result = super.deleteById(id);

        if (result){

            //添加成功  重置redis
            List<CourseType> allCourseType = baseMapper.selectList(null);
            //把可以分类缓存到Redis中
            AjaxResult allCourseTypeSetResult =
                    redisFeignClient.setString(RedisConstantis.KEY_ALLCOURSETYPE, JSON.toJSONString(allCourseType));
            if(!allCourseTypeSetResult.isSuccess()){
                throw new RuntimeException("缓存重置失败");
            }

        }

        return result;
    }

}

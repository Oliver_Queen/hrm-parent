package com.jwj.hrm.service;

import com.jwj.hrm.domain.CourseMarket;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface ICourseMarketService extends IService<CourseMarket> {

}

package com.jwj.hrm.service.impl;

import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.domain.Course;
import com.jwj.hrm.domain.CourseDetail;
import com.jwj.hrm.domain.CourseMarket;
import com.jwj.hrm.dto.CourseDto;
import com.jwj.hrm.dto.LoginInfoDto;
import com.jwj.hrm.feignclient.ESFeignClient;
import com.jwj.hrm.mapper.CourseDetailMapper;
import com.jwj.hrm.mapper.CourseMapper;
import com.jwj.hrm.mapper.CourseMarketMapper;
import com.jwj.hrm.query.CourseQuery;
import com.jwj.hrm.service.ICourseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.PageList;
import com.jwj.hrm.util.UserContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
@Service
@Transactional
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {


    @Autowired
    private CourseMarketMapper courseMarketMapper;


    @Autowired
    private CourseDetailMapper courseDetailMapper ;


    @Autowired
    private ESFeignClient esFeignClient;


    @Override
    public void saveOrUpdate(CourseDto courseDto) {
        Course course = courseDto.getCourse();
        CourseDetail courseDetail = courseDto.getCourseDetail();
        CourseMarket courseMarket = courseDto.getCourseMarket();

        //判断参数是否为空
        if (!StringUtils.hasLength(course.getName())){
            throw new RuntimeException("课程名字不能为空");
        }

        //保存课程表
        course.setStatus(Course.STATUS_OFFLINE);
        //获取当前用户
        LoginInfoDto loginInfo = UserContext.getLoginInfo();
        //机构id
        course.setTenantId(loginInfo.getTenantId());
        //机构名称
        course.setTenantName(loginInfo.getTenantName());
        //创建用户id
        course.setUserId(loginInfo.getUserId());
        //创建用户名字
        course.setUserName(loginInfo.getUsername());

        Date date = new Date();
        course.setStartTime(date);
        course.setEndTime(date);

        baseMapper.insert(course);

        //保存课程营销表
        courseMarket.setId(course.getId());
        courseMarket.setValid(0);
        courseMarket.setExpires(date);
        courseMarket.setPriceOld(courseMarket.getPrice()+500);
        courseMarketMapper.insert(courseMarket);

       //保存课程详情表
        courseDetail.setId(course.getId());
        courseDetailMapper.insert(courseDetail);





    }

    //上线课程
    @Override
    public void onLineCourse(Long id) {

        //根据id查询课程
        Course course = baseMapper.selectById(id);
        //判断课程是否为空 状态必须是下线
        if (course==null || course.getStatus()!=0){
            throw new RuntimeException("找不到课程，或者课程已上线");
        }

        CourseMarket courseMarket = courseMarketMapper.selectById(id);

        //课程详情
        CourseDetail courseDetail = courseDetailMapper.selectById(id);

        CourseDoc courseDoc = new CourseDoc();

        //属性拷贝
        BeanUtils.copyProperties(course ,courseDoc);
        BeanUtils.copyProperties(courseDetail ,courseDoc);
        BeanUtils.copyProperties(courseMarket ,courseDoc);

        //调用ES的Feign添加CourseDoc数据到ES
        AjaxResult saveAjaxResult = esFeignClient.save(courseDoc);
        if(!saveAjaxResult.isSuccess()){
            throw new RuntimeException("课程上线失败["+ saveAjaxResult.getMessage()+"]");
        }

        //修改数据库中es状态为上线
        course.setStatus(1);
        baseMapper.updateById(course);

    }

    @Override
    public void offLineCourse(Long id) {

        //删除ES中的课程
        AjaxResult ajaxResult = esFeignClient.deleteById(id);
        if(!ajaxResult.isSuccess()){
            throw new RuntimeException("课程下线失败");
        }

        //修改Mysql中的课程状态
        baseMapper.updateStatusById(id , 0);
    }

    @Override
    public PageList<CourseDoc> queryCourses(CourseQuery courseQuery) {


        return esFeignClient.searchCourse(courseQuery);
    }
}

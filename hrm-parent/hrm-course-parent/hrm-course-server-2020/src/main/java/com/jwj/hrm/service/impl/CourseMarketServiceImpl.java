package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.CourseMarket;
import com.jwj.hrm.mapper.CourseMarketMapper;
import com.jwj.hrm.service.ICourseMarketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
@Service
public class CourseMarketServiceImpl extends ServiceImpl<CourseMarketMapper, CourseMarket> implements ICourseMarketService {

}

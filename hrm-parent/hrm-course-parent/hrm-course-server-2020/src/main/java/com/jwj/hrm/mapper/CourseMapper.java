package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Course;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import feign.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface CourseMapper extends BaseMapper<Course> {
    //修改课程状态
    int updateStatusById(@Param("id") Long id, @Param("status") int status);
}

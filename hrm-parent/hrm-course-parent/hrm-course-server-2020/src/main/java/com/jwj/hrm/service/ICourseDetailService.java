package com.jwj.hrm.service;

import com.jwj.hrm.domain.CourseDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public interface ICourseDetailService extends IService<CourseDetail> {

}

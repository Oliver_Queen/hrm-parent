package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.CourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程目录 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-24
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {

    List<CourseType> selectByPid(Long pid, Long id);
}

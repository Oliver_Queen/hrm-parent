package com.jwj.hrm.query;


/**
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
public class CourseQuery extends BaseQuery{

    //最大价格
    private Float priceMax;
    //最小价格
    private Float priceMin;
    //课程类型
    private Long productType;

    public Float getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Float priceMax) {
        this.priceMax = priceMax;
    }

    public Float getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Float priceMin) {
        this.priceMin = priceMin;
    }

    public Long getProductType() {
        return productType;
    }

    public void setProductType(Long productType) {
        this.productType = productType;
    }

}
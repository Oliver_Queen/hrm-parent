package com.jwj.hrm.vo;


import com.jwj.hrm.domain.CourseType;

import java.util.ArrayList;
import java.util.List;

//面包屑节点对象
public class CrumbsVo {

    //每个节点对象
    private CourseType ownerProductType;

    //每个节点对象的下拉
    private List<CourseType> otherProductTypes = new ArrayList<>();

    public CourseType getOwnerProductType() {
        return ownerProductType;
    }

    public void setOwnerProductType(CourseType ownerProductType) {
        this.ownerProductType = ownerProductType;
    }

    public List<CourseType> getOtherProductTypes() {
        return otherProductTypes;
    }

    public void setOtherProductTypes(List<CourseType> otherProductTypes) {
        this.otherProductTypes = otherProductTypes;
    }
}

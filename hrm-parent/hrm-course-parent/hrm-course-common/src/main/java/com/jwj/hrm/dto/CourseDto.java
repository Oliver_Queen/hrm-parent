package com.jwj.hrm.dto;

import com.jwj.hrm.domain.Course;
import com.jwj.hrm.domain.CourseDetail;
import com.jwj.hrm.domain.CourseMarket;

public class CourseDto {

    private Course course;
    private CourseDetail courseDetail;
    private CourseMarket courseMarket;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public CourseDetail getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(CourseDetail courseDetail) {
        this.courseDetail = courseDetail;
    }

    public CourseMarket getCourseMarket() {
        return courseMarket;
    }

    public void setCourseMarket(CourseMarket courseMarket) {
        this.courseMarket = courseMarket;
    }
}

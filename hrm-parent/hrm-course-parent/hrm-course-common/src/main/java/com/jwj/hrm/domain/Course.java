package com.jwj.hrm.domain;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author weijia.jin
 * @since 2020-03-28
 */
@TableName("t_course")
public class Course extends Model<Course> {

    //课程状态0未上线 ， 1 已经上线
    public static final int STATUS_OFFLINE = 0;
    public static final int STATUS_ONLINE = 1;

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 课程名称
     */
    private String name;
    /**
     * 适用人群
     */
    private String users;
    /**
     * 课程大分类
     */
    @TableField("course_type_id")
    private Long courseTypeId;
    private String gradeName;
    /**
     * 课程等级
     */
    @TableField("grade_id")
    private Long gradeId;
    /**
     * 课程状态0未上线 ， 1 已经上线
     */
    private Integer status;
    /**
     * 教育机构
     */
    @TableField("tenant_id")
    private Long tenantId;
    /**
     * 冗余
     */
    private String tenantName;
    /**
     * 创建用户，后台谁创建的课程
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 创建用户，后台谁创建的课程
     */
    private String userName;
    /**
     * 课程的开始时间
     */
    @TableField("start_time")
    private Date startTime;
    @TableField("end_time")
    private Date endTime;
    /**
     * 封面
     */
    private String pic;

    //购买数量
    private Integer buyNumber = 0;
    //浏览数量
    private Integer browseNumber = 0;
    //评论数
    private Integer commentNumber = 0;


    public Integer getBuyNumber() {
        return buyNumber;
    }

    public void setBuyNumber(Integer buyNumber) {
        this.buyNumber = buyNumber;
    }

    public Integer getBrowseNumber() {
        return browseNumber;
    }

    public void setBrowseNumber(Integer browseNumber) {
        this.browseNumber = browseNumber;
    }

    public Integer getCommentNumber() {
        return commentNumber;
    }

    public void setCommentNumber(Integer commentNumber) {
        this.commentNumber = commentNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public Long getCourseTypeId() {
        return courseTypeId;
    }

    public void setCourseTypeId(Long courseTypeId) {
        this.courseTypeId = courseTypeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Course{" +
                ", id=" + id +
                ", name=" + name +
                ", users=" + users +
                ", courseTypeId=" + courseTypeId +
                ", gradeName=" + gradeName +
                ", gradeId=" + gradeId +
                ", status=" + status +
                ", tenantId=" + tenantId +
                ", tenantName=" + tenantName +
                ", userId=" + userId +
                ", userName=" + userName +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", pic=" + pic +
                "}";
    }
}

package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipRealinfo;
import com.jwj.hrm.mapper.VipRealinfoMapper;
import com.jwj.hrm.service.IVipRealinfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员实名资料 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipRealinfoServiceImpl extends ServiceImpl<VipRealinfoMapper, VipRealinfo> implements IVipRealinfoService {

}

package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员基本信息 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipBaseMapper extends BaseMapper<VipBase> {

}

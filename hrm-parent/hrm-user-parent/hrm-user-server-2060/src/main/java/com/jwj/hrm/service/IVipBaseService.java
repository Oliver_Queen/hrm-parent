package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipBase;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员基本信息 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipBaseService extends IService<VipBase> {

}

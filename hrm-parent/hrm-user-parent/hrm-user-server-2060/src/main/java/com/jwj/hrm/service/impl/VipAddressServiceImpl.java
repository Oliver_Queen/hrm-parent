package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipAddress;
import com.jwj.hrm.mapper.VipAddressMapper;
import com.jwj.hrm.service.IVipAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipAddressServiceImpl extends ServiceImpl<VipAddressMapper, VipAddress> implements IVipAddressService {

}

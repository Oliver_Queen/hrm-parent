package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 收货地址 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipAddressMapper extends BaseMapper<VipAddress> {

}

package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipCourseView;
import com.jwj.hrm.mapper.VipCourseViewMapper;
import com.jwj.hrm.service.IVipCourseViewService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品浏览 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipCourseViewServiceImpl extends ServiceImpl<VipCourseViewMapper, VipCourseView> implements IVipCourseViewService {

}

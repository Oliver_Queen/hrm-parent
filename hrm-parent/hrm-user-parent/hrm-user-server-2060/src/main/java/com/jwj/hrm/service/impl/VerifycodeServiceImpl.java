package com.jwj.hrm.service.impl;

import com.alibaba.fastjson.JSON;
import com.jwj.hrm.dto.SendSMSCodeDto;
import com.jwj.hrm.feignclient.RedisFeignClient;
import com.jwj.hrm.service.IVerifycodeService;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.HttpUtil;
import com.jwj.hrm.util.StrUtils;
import com.jwj.hrm.util.VerifyCodeUtils;
import com.jwj.hrm.vo.SMSCodeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class VerifycodeServiceImpl implements IVerifycodeService {

    @Autowired
    private RedisFeignClient redisFeignClient ;

    //创建图片验证码
    @Override
    public String createImageCode(String imageCodeKey) {

        //1.判断参数
        if (!StringUtils.hasLength(imageCodeKey)){
            throw new RuntimeException("imageCodeKey无效");
        }

        //2.图片验证码的值创建 imageCodeValue
        String imageCodeValue = UUID.randomUUID().toString().substring(0, 4);
        //3.把图片验证码的值放到Redis , 以 imageCodeKey作为key :imageCodeKey =  imageCodeValue
        AjaxResult imageCodeResult = redisFeignClient.setex(imageCodeKey, 300, imageCodeValue);
        if(!imageCodeResult.isSuccess()){
            throw new RuntimeException("验证码获取失败");
        }
        //4.把 imageCodeValue 合并成图片,然后把图片基于base64编码成字符串
        String base64String = VerifyCodeUtils.verifyCode(150, 50, imageCodeValue);
        //5.返回base64编码成字符串

        return base64String;
    }

    @Override
    public void sendSmsCode(SendSMSCodeDto sendSMSCodeDto) {
        //前台传过来的图片验证码
        String imageCode = sendSMSCodeDto.getImageCode();
        //图片验证码Redis中的key
        String imageCodeKey = sendSMSCodeDto.getImageCodeKey();
        //手机号码
        String mobile = sendSMSCodeDto.getMobile();
        //1.判断参数
        if(!StringUtils.hasLength(imageCode)){
            throw new RuntimeException("图片验证码错误，刷新后重新输入试试");
        }

        if(!StringUtils.hasLength(imageCodeKey)){
            throw new RuntimeException("图片验证码错误，刷新后重新输入试试");
        }

        if(!StringUtils.hasLength(mobile)){
            throw new RuntimeException("手机号码错误");
        }

        //2.判断图片验证码是否正确，并且获取redis中的图片验证码和前台传入的验证码进行比较
        AjaxResult imageCodeAjaxResult = redisFeignClient.getString(imageCodeKey);
        if(!imageCodeAjaxResult.isSuccess() || imageCodeAjaxResult.getResultObj() == null){
            throw new RuntimeException("图片验证码错误，刷新后重新输入试试");
        }

        //从redis中获取图片验证码的值
        String imageCodeValueFromRedis = imageCodeAjaxResult.getResultObj().toString();
        if(!imageCodeValueFromRedis.toLowerCase().trim().equals(imageCode.toLowerCase().trim())){
            throw new RuntimeException("图片验证码错误，刷新后重新输入试试");
        }

        //当前发送时间
        Date now = new Date();

        //手机验证码的值
        String SMSCodeValue = null;

        //验证码发送记录在Redis的key
        String SMSCodeKeyInRedis = "sms:"+mobile;

        //3.获取上一次的手机验证码发送记录
        AjaxResult lastSMSCodeResult = redisFeignClient.getString(SMSCodeKeyInRedis);

        //判断上一次是否发生
        if(lastSMSCodeResult.isSuccess() && lastSMSCodeResult.getResultObj() != null){
            //3.1.已发送
            SMSCodeVo smsCodeVo = JSON.parseObject(lastSMSCodeResult.getResultObj().toString(), SMSCodeVo.class);
            //1.判断上一次发送时间和现在时间是否发送频繁：上一次发送时间和当前发送时间差要大于 60s
            Date lastSendDate = smsCodeVo.getDate();
            //2.如果没有超过60s，报异常，返回提示信息
            if(now.getTime()/1000 - lastSendDate.getTime()/1000 <  60){
                throw new RuntimeException("手速太快了，等会再试吧");
            }
            //3.如果过了60s,获取上一次发送的证码的值
            SMSCodeValue = smsCodeVo.getCode();

        }else{
            //3.2.未发送
            //1.创建手机验证码
            SMSCodeValue = StrUtils.getRandomString(4);
        }

        //4.存储手机验证码发送记录Redis,设置过期时间5分钟


        //将验证码发送记录封装成vo，并且转换json格式
        SMSCodeVo smsCodeVo = new SMSCodeVo(SMSCodeValue, now);
        String smsCodeJson = JSON.toJSONString(smsCodeVo);

        //手机验证码发送记录Redis
        AjaxResult smsCodeResult = redisFeignClient.setex(SMSCodeKeyInRedis, 600, smsCodeJson);
        if(!smsCodeResult.isSuccess()){
            throw new RuntimeException("手机验证码发送失败[缓存服务不可用]，请稍后重试");
        }

        //5.发送验证码到目标手机(短信网关)
        String messageContent = "您的手机验证码为["+SMSCodeValue+"]，验证码将在5分钟后失效";
        System.out.println(messageContent);

        Map<String, String> map = new HashMap<>();
        map.put("Uid","Oliverr");
        map.put("Key","d41d8cd98f00b204e980");
        map.put("smsMob",mobile); //接收的手机号
        map.put("smsText",messageContent);

        //发送短信
        String result = HttpUtil.sendPost("http://utf8.api.smschinese.cn/",map);
        if(StringUtils.hasLength(result) && Integer.valueOf(result) < 0){
           if(Integer.valueOf(result) == -1){
               throw new RuntimeException("发送失败[没有该账户，请联系管理员]");
            }
            throw new RuntimeException("发送失败[错误码："+result+"，请联系管理员]");
        }

        //保存一个发送记录到mysql:insert

        //6.返回结果，发送成功
    }
}

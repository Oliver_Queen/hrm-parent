package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipLoginLog;
import com.jwj.hrm.mapper.VipLoginLogMapper;
import com.jwj.hrm.service.IVipLoginLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录记录 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipLoginLogServiceImpl extends ServiceImpl<VipLoginLogMapper, VipLoginLog> implements IVipLoginLogService {

}

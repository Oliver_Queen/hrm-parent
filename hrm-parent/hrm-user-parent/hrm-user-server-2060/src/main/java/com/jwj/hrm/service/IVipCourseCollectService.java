package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipCourseCollect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品收藏 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipCourseCollectService extends IService<VipCourseCollect> {

}

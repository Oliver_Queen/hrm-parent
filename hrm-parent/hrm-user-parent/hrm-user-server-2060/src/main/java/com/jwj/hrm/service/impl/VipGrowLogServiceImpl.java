package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipGrowLog;
import com.jwj.hrm.mapper.VipGrowLogMapper;
import com.jwj.hrm.service.IVipGrowLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成长值记录 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipGrowLogServiceImpl extends ServiceImpl<VipGrowLogMapper, VipGrowLog> implements IVipGrowLogService {

}

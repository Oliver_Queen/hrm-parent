package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipRealinfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员实名资料 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipRealinfoService extends IService<VipRealinfo> {

}

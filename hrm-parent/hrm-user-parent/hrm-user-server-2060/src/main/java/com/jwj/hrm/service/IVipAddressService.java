package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipAddress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipAddressService extends IService<VipAddress> {

}

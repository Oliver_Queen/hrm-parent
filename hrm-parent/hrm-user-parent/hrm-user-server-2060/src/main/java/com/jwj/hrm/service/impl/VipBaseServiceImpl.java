package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipBase;
import com.jwj.hrm.mapper.VipBaseMapper;
import com.jwj.hrm.service.IVipBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员基本信息 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipBaseServiceImpl extends ServiceImpl<VipBaseMapper, VipBase> implements IVipBaseService {

}

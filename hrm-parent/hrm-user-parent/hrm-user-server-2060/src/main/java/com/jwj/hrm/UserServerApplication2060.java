package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 用户服务启动类
 */
@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients({"com.jwj.hrm.feignclient"})
public class UserServerApplication2060
{
    public static void main( String[] args )
    {
        SpringApplication.run(UserServerApplication2060.class);
    }
}

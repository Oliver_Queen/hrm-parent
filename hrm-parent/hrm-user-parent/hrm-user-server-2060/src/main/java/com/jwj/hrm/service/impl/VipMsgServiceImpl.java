package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipMsg;
import com.jwj.hrm.mapper.VipMsgMapper;
import com.jwj.hrm.service.IVipMsgService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站内信 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipMsgServiceImpl extends ServiceImpl<VipMsgMapper, VipMsg> implements IVipMsgService {

}

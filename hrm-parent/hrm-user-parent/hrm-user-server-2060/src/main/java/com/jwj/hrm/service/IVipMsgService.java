package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipMsg;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 站内信 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipMsgService extends IService<VipMsg> {

}

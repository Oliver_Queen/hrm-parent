package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipCourseCollect;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品收藏 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipCourseCollectMapper extends BaseMapper<VipCourseCollect> {

}

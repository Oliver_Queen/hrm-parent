package com.jwj.hrm.service.impl;

import com.jwj.hrm.domain.VipCourseCollect;
import com.jwj.hrm.mapper.VipCourseCollectMapper;
import com.jwj.hrm.service.IVipCourseCollectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品收藏 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class VipCourseCollectServiceImpl extends ServiceImpl<VipCourseCollectMapper, VipCourseCollect> implements IVipCourseCollectService {

}

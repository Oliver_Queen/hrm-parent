package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipRealinfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员实名资料 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipRealinfoMapper extends BaseMapper<VipRealinfo> {

}

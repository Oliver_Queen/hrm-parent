package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipCourseView;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品浏览 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipCourseViewMapper extends BaseMapper<VipCourseView> {

}

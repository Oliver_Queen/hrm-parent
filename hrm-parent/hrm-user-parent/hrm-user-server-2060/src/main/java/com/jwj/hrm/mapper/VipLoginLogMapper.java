package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 登录记录 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipLoginLogMapper extends BaseMapper<VipLoginLog> {

}

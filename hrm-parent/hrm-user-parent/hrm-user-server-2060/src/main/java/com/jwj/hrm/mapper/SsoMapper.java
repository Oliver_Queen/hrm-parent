package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.Sso;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登录账号 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface SsoMapper extends BaseMapper<Sso> {

    Sso selectByPhone(String mobile);
}

package com.jwj.hrm.web.controller;

import com.jwj.hrm.dto.SendSMSCodeDto;
import com.jwj.hrm.service.IVerifycodeService;
import com.jwj.hrm.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class VerifycodeController {

    @Autowired
    private IVerifycodeService verifycodeService ;

    //获取图片验证码
    @GetMapping("/verifycode/imageCode/{imageCodeKey}")
    public String createImageCode(@PathVariable("imageCodeKey") String imageCodeKey){
        return verifycodeService.createImageCode(imageCodeKey);
    }


    //获取手机验证码
    @PostMapping("/verifycode/sendSmsCode")
    public AjaxResult sendSmsCode(@RequestBody SendSMSCodeDto sendSMSCodeDto){
        try{
            verifycodeService.sendSmsCode(sendSMSCodeDto);
        }catch (RuntimeException e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("服务器异常");
        }
        return AjaxResult.me();
    }

}

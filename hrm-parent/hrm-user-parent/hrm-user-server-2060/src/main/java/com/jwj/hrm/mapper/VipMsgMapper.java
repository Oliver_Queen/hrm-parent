package com.jwj.hrm.mapper;

import com.jwj.hrm.domain.VipMsg;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 站内信 Mapper 接口
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface VipMsgMapper extends BaseMapper<VipMsg> {

}

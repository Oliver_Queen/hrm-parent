package com.jwj.hrm.service;

import com.jwj.hrm.domain.Sso;
import com.baomidou.mybatisplus.service.IService;
import com.jwj.hrm.dto.RegistryDto;

/**
 * <p>
 * 会员登录账号 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface ISsoService extends IService<Sso> {

    //注册
    void register(RegistryDto registryDto);
    //登陆
    String login(Sso sso);
}

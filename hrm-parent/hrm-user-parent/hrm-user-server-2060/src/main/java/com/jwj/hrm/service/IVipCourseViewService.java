package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipCourseView;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品浏览 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipCourseViewService extends IService<VipCourseView> {

}

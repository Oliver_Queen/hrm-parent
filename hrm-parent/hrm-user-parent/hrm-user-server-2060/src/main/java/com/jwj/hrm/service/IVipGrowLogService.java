package com.jwj.hrm.service;

import com.jwj.hrm.domain.VipGrowLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 成长值记录 服务类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
public interface IVipGrowLogService extends IService<VipGrowLog> {

}

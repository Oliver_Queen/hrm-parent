package com.jwj.hrm.service;

import com.jwj.hrm.dto.SendSMSCodeDto;

public interface IVerifycodeService {
    String createImageCode(String imageCodeKey);

    void sendSmsCode(SendSMSCodeDto sendSMSCodeDto);
}

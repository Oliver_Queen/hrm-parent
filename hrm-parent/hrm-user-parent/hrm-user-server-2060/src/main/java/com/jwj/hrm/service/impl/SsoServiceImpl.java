package com.jwj.hrm.service.impl;

import com.alibaba.fastjson.JSON;
import com.jwj.hrm.domain.Sso;
import com.jwj.hrm.domain.VipBase;
import com.jwj.hrm.dto.RegistryDto;
import com.jwj.hrm.feignclient.RedisFeignClient;
import com.jwj.hrm.mapper.SsoMapper;
import com.jwj.hrm.mapper.VipBaseMapper;
import com.jwj.hrm.service.ISsoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.encrypt.MD5;
import com.jwj.hrm.vo.SMSCodeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 会员登录账号 服务实现类
 * </p>
 *
 * @author weijia.jin
 * @since 2020-04-05
 */
@Service
public class SsoServiceImpl extends ServiceImpl<SsoMapper, Sso> implements ISsoService {


    @Autowired
    private RedisFeignClient redisFeignClient;

    @Autowired
    private VipBaseMapper vipBaseMapper;

    @Override
    public void register(RegistryDto registryDto) {

        //1.获取基本参数
        String mobile = registryDto.getMobile();
        String password = registryDto.getPassword();
        String smsCode = registryDto.getSmsCode();

        //2.判断手机号是否被注册
        Sso ssoInMysql = baseMapper.selectByPhone(mobile);
        if(ssoInMysql != null){
            throw new RuntimeException("手机号已注册");
        }

        //3.判断手机验证码是否正确

        String SMSCodeKeyInRedis = "sms:"+mobile;
        //3.1.根据 sms:手机号  作为key从Redis中获取验证码发送记录
        AjaxResult lastSMSCodeResult = redisFeignClient.getString(SMSCodeKeyInRedis);
        if(!lastSMSCodeResult.isSuccess() || lastSMSCodeResult.getResultObj() == null){
            throw new RuntimeException("无法获取手机验证码，请重新发送");
        }

        //3.2.判断redis中的验证码和传入的验证码是否一致
        SMSCodeVo smsCodeVo = JSON.parseObject(lastSMSCodeResult.getResultObj().toString(), SMSCodeVo.class);
        if(!smsCodeVo.getCode().equals(smsCode.trim())){
            throw new RuntimeException("手机验证码错误，请重新发送");
        }

        long time = new Date().getTime();
        //4.保存用户信息：t_sso
        Sso sso = new Sso();
        sso.setPhone(mobile);
        sso.setCreateTime(time);

        //密码加密，创建盐值
        String salt = MD5.getRandomCode(16);
        sso.setSalt(salt);
        String md5Password = MD5.getMD5(password + salt);
        sso.setPassword(md5Password);

        baseMapper.insert(sso);


        //5.初始化t_vip_base表
        VipBase vipBase = new VipBase();
        vipBase.setCreateTime(time);
        vipBase.setSsoId(sso.getId());

        //0：网站注册    1：手机注册
        vipBase.setRegChannel(0);
        vipBase.setRegTime(time);
        vipBaseMapper.insert(vipBase);
    }

    @Override
    public String login(Sso sso) {
        //1.判断基本参数
        if(!StringUtils.hasLength(sso.getPassword()) || !StringUtils.hasLength(sso.getPhone()) ){
            throw new RuntimeException("用户名密码无效");
        }
        //2.根据用户名查询SSO
        Sso ssoFromMysql = baseMapper.selectByPhone(sso.getPhone());
        if (ssoFromMysql ==null){
            throw new RuntimeException("用户名不存在");
        }
        //3.对比密码
        String password = ssoFromMysql.getPassword();
        String md5Password = MD5.getMD5(sso.getPassword() + ssoFromMysql.getSalt());
        if (!password.equals(md5Password)){
            throw new RuntimeException("密码错误");
        }

        //4.把登录信息放到redis
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        ssoFromMysql.setPassword("");
        AjaxResult result = redisFeignClient.setex(token, 60 * 60, JSON.toJSONString(ssoFromMysql));
        if (!result.isSuccess()){
            throw new RuntimeException("登陆失败");
        }
        //5.把redis的key返回
        return token;

    }
}

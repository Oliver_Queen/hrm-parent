package com.jwj.hrm.dto;

//接收短信码发送参数
public class SendSMSCodeDto {

    private String imageCode;

    private String imageCodeKey;

    private String mobile;

    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getImageCodeKey() {
        return imageCodeKey;
    }

    public void setImageCodeKey(String imageCodeKey) {
        this.imageCodeKey = imageCodeKey;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}

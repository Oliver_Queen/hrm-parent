package com.jwj.hrm.vo;

import java.util.Date;

public class SMSCodeVo {

    private String code;

    private Date date;

    public SMSCodeVo() {
    }

    public SMSCodeVo(String code, Date date) {
        this.code = code;
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

package com.jwj.hrm.filter;


import com.alibaba.fastjson.JSON;
import com.jwj.hrm.feignclient.RedisFeignClient;
import com.jwj.hrm.util.AjaxResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoginCheckFilter extends ZuulFilter {

    @Autowired
    private RedisFeignClient redisFeignClient ;

    //private static final Logger log = LoggerFactory.getLogger(LoginCheckFilter.class);

    //执行顺序
    private static final int ORDER = 1;

    //filter类型 : "pre"前置
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    //执行顺序
    @Override
    public int filterOrder() {
        return ORDER;
    }

    //返回结果决定 是否要执行run方法
    @Override
    public boolean shouldFilter() {
        // /static/**  ，/login , /register 不需要做登录检查，返回false
        //1.获取request对象 ， 获取请求中的url
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        String url = request.getRequestURI();
        //log.info("请求地址："+url);
        //2.判断是否包含在： static/**  ，/login , /register
        if(url.endsWith("/login") || url.endsWith("/register")){
            return false;
        }
        //要做登录检查的返回true
        return true;
    }

    //核心业务方法 ： 登录检查 ， 如果请求头中有token，就是登录成功
    @Override
    public Object run() {
        //1.获取请求对象
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();

        //响应对象
        HttpServletResponse response = RequestContext.getCurrentContext().getResponse();


        //2.获取请求头中的 token
        String token = request.getHeader("token");

        //3.如果没有token，登录检查失败 ，
        if(!StringUtils.hasLength(token)){
            //检查失败
            checkError(response);
        }else{
            //去Redis中判断这个token是否存在
            AjaxResult loginInRedisResult = redisFeignClient.getString(token);
            if(!loginInRedisResult.isSuccess() || loginInRedisResult.getResultObj() == null){
                //检查失败
                checkError(response);
            }
        }
        //这里的返回结果没有任何意义，不用管
        return null;
    }

    private void checkError(HttpServletResponse response) {
        //3.1.返回登录检查失败的错误信息 :{ success:false, message:"登录检查失败，请重新登录"}
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("success" , false);
        resultMap.put("message" , "登录检查失败，请重新登录");
        //中文编码
        response.setContentType("application/json;charset=utf-8");
        //把map转成json字符串，写到浏览器
        String resultJsonString = JSON.toJSONString(resultMap);

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        try {
            response.getWriter().print(resultJsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //3.2.阻止filter继续往后执行
        RequestContext.getCurrentContext().setSendZuulResponse(false);
    }
}

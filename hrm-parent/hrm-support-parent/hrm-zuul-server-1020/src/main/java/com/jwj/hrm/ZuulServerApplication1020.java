package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * zuul启动类
 */
//开启Zuul
@EnableZuulProxy
@SpringBootApplication
@EnableFeignClients("com.jwj.hrm.feignclient")
public class ZuulServerApplication1020
{
    public static void main( String[] args )
    {
        SpringApplication.run(ZuulServerApplication1020.class);
    }
}

package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 配置中心
 */
@SpringBootApplication
//开启配置中心
@EnableConfigServer
public class ConfigServerApplication1030
{
    public static void main( String[] args )
    {
        SpringApplication.run(ConfigServerApplication1030.class);
    }
}

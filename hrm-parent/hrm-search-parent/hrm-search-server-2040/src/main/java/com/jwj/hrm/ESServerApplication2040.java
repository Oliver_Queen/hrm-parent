package com.jwj.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ESServerApplication2040
{
    public static void main( String[] args )
    {
        SpringApplication.run(ESServerApplication2040.class);
    }
}
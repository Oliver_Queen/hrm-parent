package com.jwj.hrm.test;


import com.jwj.hrm.ESServerApplication2040;
import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.repository.CourseESRepository;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ESServerApplication2040.class)
public class ESTest {

    //操作ES的工具
    @Autowired
    private ElasticsearchTemplate template;

    @Autowired
    private CourseESRepository courseESRepository;


    //初始化索引库
    @Test
    public void testInit(){
        //创建索引
        template.createIndex(CourseDoc.class);

        //设置映射
        template.putMapping(CourseDoc.class);
    }


    //es添加
    @Test
    public void testAdd(){

            CourseDoc doc = new CourseDoc();
            doc.setId(1L);
            doc.setName("Java基础班");
            doc.setGradeId(10L);
            courseESRepository.save(doc);


    }

    //es查询
    @Test
    public void testGet(){
        //处理空指针
        Optional<CourseDoc> optional = courseESRepository.findById(1L);
        //get获取真正的结果
        CourseDoc courseDoc = optional.get();
        System.out.println(courseDoc);
    }

    //ES修改
    @Test
    public void testUpdate(){

        CourseDoc doc = new CourseDoc();
        doc.setId(1L);
        doc.setName("PHP大神班");
        doc.setGradeId(2L);
        courseESRepository.save(doc);
    }

    //ES删除
    @Test
    public void testDelete(){
        courseESRepository.deleteById(1L);
    }


    //对ES做添加
    @Test
    public void testAddd(){

        //修改跟添加是一样的，只要有ID就是修改
        for(int i = 0 ; i < 40 ; i++){
            CourseDoc doc = new CourseDoc();
            doc.setId(Long.valueOf(i+2));

            if(i % 2 == 0){
                doc.setName("Java大神班");
                doc.setGradeId(1L);
            }else{
                doc.setName("PHP基础班");
                doc.setGradeId(2L);
            }
            Float price = 2.0f * i;
            doc.setPrice(price+100);
            courseESRepository.save(doc);
        }

    }






    @Test
    public void testQuery(){
       //准备查询对象
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();

        //查询条件
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //DSL查询
        List<QueryBuilder> must = boolQueryBuilder.must();
        must.add(QueryBuilders.matchQuery("name","java"));

        //DSL过滤
        List<QueryBuilder> filter = boolQueryBuilder.filter();
        filter.add(QueryBuilders.termQuery("gradeId",1L));
        filter.add(QueryBuilders.rangeQuery("price").lte(150).gte(100));

        queryBuilder.withQuery(boolQueryBuilder);
        //分页
        NativeSearchQueryBuilder queryBuilder1 = queryBuilder.withPageable(PageRequest.of(1,5));

        //排序  根据价格升序
        queryBuilder1.withSort(new FieldSortBuilder("price").order(SortOrder.ASC));

        SearchQuery searchQuery =queryBuilder.build();
        //查询
        Page<CourseDoc> page = courseESRepository.search(searchQuery);



        //处理结果
        long count = page.getTotalElements();
        List<CourseDoc> list = page.getContent();
        //遍历
        list.forEach(e->{
            System.out.println(e);
        });

    }
}

package com.jwj.hrm.repository;


import com.jwj.hrm.doc.CourseDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CourseESRepository extends ElasticsearchRepository<CourseDoc,Long> {
}

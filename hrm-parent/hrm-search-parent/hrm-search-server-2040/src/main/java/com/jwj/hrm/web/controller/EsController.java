package com.jwj.hrm.web.controller;


import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.query.CourseQuery;
import com.jwj.hrm.repository.CourseESRepository;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.PageList;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EsController {

    @Autowired
    private CourseESRepository courseESRepository;

    //保存课程
    @RequestMapping("/es/save")
    public AjaxResult save(@RequestBody CourseDoc courseDoc){
        courseESRepository.save(courseDoc);
        return AjaxResult.me();
    }

    //删除
    @RequestMapping("/es/delete/{id}")
    public AjaxResult deleteById(@PathVariable("id") Long id){
        courseESRepository.deleteById(id);
        return AjaxResult.me();
    }



    //课程搜索
    @RequestMapping(value = "/es/searchCourse",method = RequestMethod.POST)
    public PageList<CourseDoc> searchCourse(@RequestBody CourseQuery courseQuery){
        //把courseQuery封装成 ES的查询对象
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();

        //组合查询
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        List<QueryBuilder> must = boolQueryBuilder.must();
        List<QueryBuilder> filter = boolQueryBuilder.filter();

        //关键字
        if(StringUtils.hasLength(courseQuery.getKeyword())){
            must.add(QueryBuilders.matchQuery("name",courseQuery.getKeyword()));
        }

        //课程类型
        if(null != courseQuery.getProductType()){
            filter.add(QueryBuilders.termQuery("courseTypeId",courseQuery.getProductType()));
        }

        //最大价格
        if(null != courseQuery.getPriceMax()){
            filter.add(QueryBuilders.rangeQuery("price").lte(courseQuery.getPriceMax()));
        }

        //最小价格
        //private Float priceMin;-> range(范围)
        if(null != courseQuery.getPriceMin()){
            filter.add(QueryBuilders.rangeQuery("price").gte(courseQuery.getPriceMin()));
        }

        queryBuilder.withQuery(boolQueryBuilder);

        //构建分页
        queryBuilder.withPageable(PageRequest.of(courseQuery.getPage()-1,courseQuery.getRows()));
        //构建排序
        String sortFieldSn = courseQuery.getSortField();
        if(StringUtils.hasLength(sortFieldSn)){
            //排序字段
            String sortField = null;
            //排序方式：默认倒排
            SortOrder sortType = SortOrder.DESC;
            //根据查询编号确定排序字段
            switch (sortFieldSn.toLowerCase()){
                case "xl": sortField = "buyNumber";break;
                case "xp": sortField = "onlineTime";break;
                case "pl": sortField = "commentNumber";break;
                case "jg": sortField = "price";break;
                case "rq": sortField = "browseNumber";break;
            }
            if (StringUtils.hasLength(courseQuery.getSortType())
                    && "asc".equals(courseQuery.getSortType().toLowerCase())){
                sortType = SortOrder.ASC;
            }
            queryBuilder.withSort(new FieldSortBuilder(sortField).order(sortType));
        }
        SearchQuery searchQuery = queryBuilder.build();
        //执行查询
        Page<CourseDoc> page = courseESRepository.search(searchQuery);

        //返回结果
        return new PageList<CourseDoc>(page.getTotalElements() , page.getContent());
    }

}

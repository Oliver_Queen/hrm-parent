package com.jwj.hrm.fallback;


import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.feignclient.ESFeignClient;
import com.jwj.hrm.query.CourseQuery;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.PageList;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class ESFeignClientFallbackFactory implements FallbackFactory<ESFeignClient> {
    @Override
    public ESFeignClient create(Throwable throwable) {
        return new ESFeignClient(){
            @Override
            public AjaxResult save(CourseDoc courseDoc) {
                throwable.printStackTrace();
                return AjaxResult.me().setSuccess(false).setMessage("搜索服务不可用");
            }

            @Override
            public AjaxResult deleteById(Long id) {
                throwable.printStackTrace();
                return AjaxResult.me().setSuccess(false).setMessage("搜索服务不可用");
            }

            @Override
            public PageList<CourseDoc> searchCourse(CourseQuery courseQuery) {
                return new PageList<>();
            }
        };
    }
}

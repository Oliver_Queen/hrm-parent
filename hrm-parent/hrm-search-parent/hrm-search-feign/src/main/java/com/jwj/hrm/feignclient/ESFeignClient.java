package com.jwj.hrm.feignclient;


import com.jwj.hrm.doc.CourseDoc;
import com.jwj.hrm.fallback.ESFeignClientFallbackFactory;
import com.jwj.hrm.query.CourseQuery;
import com.jwj.hrm.util.AjaxResult;
import com.jwj.hrm.util.PageList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//调用ES服务的Feign接口
@FeignClient(value = "es-server",fallbackFactory = ESFeignClientFallbackFactory.class)
public interface ESFeignClient {
    //保存课程
    @RequestMapping("/es/save")
    AjaxResult save(@RequestBody CourseDoc courseDoc);

    @RequestMapping("/es/delete/{id}")
    public AjaxResult deleteById(@PathVariable("id") Long id);

    @RequestMapping(value = "/es/searchCourse",method = RequestMethod.POST)
    PageList<CourseDoc> searchCourse(@RequestBody CourseQuery courseQuery);

}
